<!DOCTYPE html>
<html>
@extends('layouts.app')
<head>
	<title>To Do List</title>
</head>
<body id="notebod">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item active"><a href="{{route('notes.index')}}">Note Taking</a></li>
		    <li class="breadcrumb-item" >To Do List</li>
		  </ol>
		</nav>

		

		<div class="container">
			<div class="row">
				  <div class="col-8 mt-5">
				  	<h2 class="addnote">Add Task</h2>
				
				<form action="/todos" method="POST">
						@csrf
			  			<div class="form-group">
								<label for="task" >Task</label>
								<textarea id="task" class="form-control"  cols="10"  name="task"></textarea>
							</div>
						
						

							<div class="form-group">
								<label for="task">Add Date and Time</label>
								<input type="datetime-local" name="date" id="date">
							</div>

					<button class="btn btn-primary btn-block">Submit</button>
				</form>

			</div>




				
				  	<div class="col-4">
						<table class= "table table-dark" id="tb">
								<thead>
									<th scope="col">Task</th>
									<th scope="col">Date and Time</th>
									<th scope="col">Action</th>
									<th scope="col"></th>
								</thead>
								<tbody>
								@foreach($todos as $todo)
								
								  <tr>	
									<td>{{$todo->task}}</td>
									<td>{{$todo->date}}</td>
									<td>

										<form action="/todos/{{$todo->id}}" method="POST">
											@csrf
											@method('DELETE')
											<button>Delete</button>
										</form>
								</td>
									<td><a href="/todos/{{$todo->id}}/edit">update</a></td>
								  </tr>

								 @endforeach

								
								    
											
								</tbody>
						</table>
		


				</div>
			</div>
		</div>
</body>
</html>