<!DOCTYPE html>
<html>
@extends('layouts.app')
<head>
	<title></title>
</head>
<body id="notebod">


	<div class="container">
			<div class="row">
				  <div class="col-8 mt-5">
				  	<h2 class="addnote">Edit Task</h2>
				
				<form action="/todos" method="POST">
						@csrf
			  			<div class="form-group">
								<label for="task">Task</label>
								<textarea id="task" class="form-control"  cols="10"  name="task">{{$todo->task}}</textarea>
							</div>
						
						

							<div class="form-group">
								<label for="task">Add Date and Time</label>
								<input class="form-control"  type="datetime" name="date" id="date" value="{{$todo->date}}">

							</div>

					<button class="btn btn-primary btn-block">Submit</button>
				</form>

			</div>
		</div>
</body>
</html>