<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;
use DB;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $notes = Note::all();
        $notes = DB::table('notes')->orderBy('created_at', 'desc')->paginate(5);
        return view('notes.index')->with("notes",$notes);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $note =  new Note;
        $note->title = $request->input('title');
        $note->text = $request->input('text');
        $note->save();

         $notes = Note::all();
         $notes = DB::table('notes')->orderBy('created_at', 'desc')->paginate(5);
        return view('notes.index')->with('note',$note)->with("notes",$notes); 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function show(Note $note)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = Note::find($id);
        return view('notes.edit')->with('note', $note);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $note = Note::find($id);
        $note->title = $request->title;
        $note->text = $request->text;
        $note->save();

        $notes = DB::table('notes')->orderBy('created_at', 'desc')->paginate(5);
        return view('notes.index')->with('note',$note)->with("notes",$notes); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Note::find($id);
        $note->delete();

         $notes = Note::all();
         $notes = DB::table('notes')->orderBy('created_at', 'desc')->paginate(5);
        return view('notes.index')->with('note',$note)->with("notes",$notes); 
        
    }
}
